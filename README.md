# ArtifactGradlePlugin

The repo contains a custom Gradle plugin that does automatic token management in order to download CodeArtifact libraries. Follow below steps for seamless configuration.

1. Add `aws.plugin` file in the root folder of your app.

2. Apply the plugin by adding below line in root `build.gradle` file,  
```
apply from: 'awsplugin.gradle'
```

3. Add maven source in root `build.gradle` file. 
```groovy
allprojects {
 repositories {
  ...
    // For downloading private dependencies from AWS codeartifact
    maven {
        url 'https://kb-android-dev-741386957827.d.codeartifact.ap-south-1.amazonaws.com/maven/kb-android/'
        credentials {
            username "aws"
            password project.awsToken
        }
    }
 }
}
```

4. Configure Codeartifact credentials. (what is [codeartifact](https://www.notion.so/Proposal-for-Code-reuse-across-Khatabook-Apps-42e621e438594a85b5490be63a1eaacf)?)
    - Install CLI by executing below terminal command. For more details, visit [official doc](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html).

        ```bash
        curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
        sudo installer -pkg AWSCLIV2.pkg -target /
        ```

    - Execute `aws configure` in terminal. It will ask for details then enter below details

        ```bash
        $ aws configure
        AWS Access Key ID [None]: AKIA2ZHQ3GQBWWQGKAJ7
        AWS Secret Access Key [None]: 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
        Default region name [None]: ap-south-1
        Default output format [None]: json
        ```
